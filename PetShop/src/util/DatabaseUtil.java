package util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import controllers.MainController;
import model.Animal;
import model.Observatii;
import model.Personalmedical;
import model.Programari;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
	
	public void setUp(){
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShop");
		entityManager = entityManagerFactory.createEntityManager();
	}
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
	public void saveDoctor(Personalmedical doctor) {
		entityManager.persist(doctor);
	}
	public void saveProgramare(Programari programare) {
		entityManager.persist(programare);
	}
	public void saveObservatie(Observatii observatie) {
		entityManager.persist(observatie);
	}
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	public void closeEntityManager() {
		entityManager.close();
	}
	
//Afisarea in consola a tuturor Animalelor , Doctorilor si Programarilor
	
	public void printAllAnimals() {
		List<Animal> animalResults = entityManager.createNativeQuery("Select * from petshop.animal " , Animal.class).getResultList();
		for(Animal animal : animalResults) {
			System.out.println("Animalul " + animal.getNume() + " has ID " + animal.getIdanimal());
		}
	}
	public void printAllProgramari() {
		List<Programari> programariResults = entityManager.createNativeQuery("Select * from petshop.programari" , Programari.class).getResultList();
		for(Programari programari : programariResults) {
			System.out.println("Animalul " + programari.getAnimal().getNume() + " este programat in data de " + programari.getData() + " la ora "
					+ programari.getOra() + " la doctorul " + programari.getPersonalmedical().getVeterinar());
		}
	}
	public void printAllDoctors() {
		List<Personalmedical> doctorResults = entityManager.createNativeQuery("Select * from petshop.personalmedical" , Personalmedical.class).getResultList();
		for(Personalmedical doctor : doctorResults) {
			System.out.println("Doctorul " + doctor.getVeterinar() + " has ID " + doctor.getIdpersonalmedical() + " si este specializat in " + doctor.getSpecializare());
		}
	}
	
// Modificarea unui anumit rand din tabele
	
	public void setAnimal(Animal animal , int id, String nume , String specie , String rasa , String sex) {
		animal.setIdanimal(id);
		animal.setNume(nume);
		animal.setRasa(rasa);
		animal.setSex(sex);
		animal.setSpecie(specie);
	}
	public void setDoctor(Personalmedical doctor , int id , String nume , String specializare) {
		doctor.setIdpersonalmedical(id);
		doctor.setVeterinar(nume);
		doctor.setSpecializare(specializare);
	}
	public void setProgramare(Programari programare , int id , String data , String ora , Personalmedical doctor , Animal animal) {
		programare.setPersonalmedical(doctor);
		programare.setOra(ora);
		programare.setData(data);
		programare.setIdprogramari(id);
		programare.setAnimal(animal);
	}
	
	
// Selectia unui anumit rand din tabele	
	
	public Animal getAnimalAtPosition(int index) {
		List<Animal> animalResults = entityManager.createNativeQuery("Select * from petshop.animal where idanimal = " +index , Animal.class).getResultList();
		return animalResults.get(0);
	}
	public Personalmedical getPersonalMedicalAtPosition (int index) {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop.personalmedical where idpersonalmedical = "+ index , Personalmedical.class).getResultList();
		return results.get(0);
	}
	public Programari getProgramareAtLocation(int index) {
		List<Programari> results = entityManager.createNativeQuery("Select * from petshop.programari where idprogramari = " + index, Programari.class).getResultList();
		return results.get(0);
	}
	
	
// Crearea unui nou rand in tabele
	public void createDiagnostic (String diagTitle,String diag , Programari programare) {
		Observatii observatie = new Observatii();
		observatie.setTitlu(diagTitle);
		observatie.setObservatie(diag);
		observatie.setProgramari(programare);
		saveObservatie(observatie);
	}
	public void createProgramare( int id , String data , String ora , Personalmedical doctor , Animal animal) {
		Programari programare = new Programari();
		programare.setIdprogramari(id);
		programare.setPersonalmedical(doctor);
		programare.setData(data);
		programare.setOra(ora);
		programare.setAnimal(animal);
		saveProgramare(programare);
	}
	public void createAnimal(int id, String nume , String specie , String rasa , String sex) {
		Animal animal = new Animal();
		animal.setIdanimal(id);
		animal.setNume(nume);
		animal.setRasa(rasa);
		animal.setSex(sex);
		animal.setSpecie(specie);
		createProgramare(id, null, null, null, animal);
		saveAnimal(animal);
		animal.addProgramari(getProgramareAtLocation(id));
	}
	public void createDoctor(int id , String nume , String specializare) {
		Personalmedical doctor = new Personalmedical();
		doctor.setIdpersonalmedical(id);
		doctor.setSpecializare(specializare);
		doctor.setVeterinar(nume);
		saveDoctor(doctor);
	}
	
// Stergerea unui rand din tabele
	
	public void deleteAnimal(int id) {
		if(entityManager.createNativeQuery("select * from petshop.programari where animalulProgramat =" +id).getResultList().size() == 0) {
			entityManager.createNativeQuery("delete from petshop.animal where idanimal =" + id).executeUpdate();
		}
		else {
			List<Programari> programariResults = entityManager.createNativeQuery("select * from petshop.programari where animalulProgramat =" +id , Programari.class).getResultList();
			for(Programari programari : programariResults) {
				deleteProgramare(programari.getIdprogramari());
			}
		entityManager.createNativeQuery("delete from petshop.animal where idanimal =" + id).executeUpdate();
		}
	}
	public void deleteDoctor(int id) {
		entityManager.createNativeQuery("delete from petshop.personalmedical where idpersonalmedical = " + id).executeUpdate();
	}
	public void deleteProgramare(int id) {
		entityManager.createNativeQuery("delete from petshop.programari where idprogramari = " + id).executeUpdate();
	}
	
// Sortarea dupa data
	
	public void sortareDupaData() {
		List<Programari> results = new ArrayList<>();
		results  = entityManager.createNativeQuery("select * from petshop.programari", Programari.class).getResultList();
		results.sort(new Comparator<Programari>(){
 
            @Override
            public int compare(Programari o1, Programari o2) {
                return o1.getData().compareTo(o2.getData());
            }
		});
		for(Programari programare : results) {
			System.out.println("Animalul " + programare.getAnimal().getNume() + " este programat in data de " + programare.getData()
			+ " la ora " + programare.getOra() + " la doctorul " + programare.getPersonalmedical().getVeterinar());
		}
	}
	
//returneaza liste din baza de date
	public List<Animal> animalList(){
		List<Animal> animalList = (List <Animal>)entityManager.createNativeQuery("select * from petshop.animal" , Animal.class).getResultList();
		return animalList;
	}
	public List<Programari> programariList(){
		List<Programari> programariList = (List <Programari>)entityManager.createNativeQuery("select * from petshop.Programari" , Programari.class).getResultList();
		return programariList;
	}
	public List<Observatii> diagList(){
		List<Observatii> diagnosticsList = (List <Observatii>)entityManager.createNativeQuery("select * from petshop.observatii", Observatii.class).getResultList();
		return diagnosticsList;
	}

	
//get selected Programare
	public Programari getselectedProgramare(String data , String ora) {
		List<Programari> results = entityManager.createNativeQuery("select * from petshop.programari where data = "+ "'" + data + "'" + "and ora=" + "'" + ora + "'" , Programari.class).getResultList();
		return results.get(0);
	}
	
//get selected Diagnostic
	public Observatii getSelectedDiag(String titlu) {
		List<Observatii> results = entityManager.createNativeQuery(("select * from petshop.observatii where titlu = '"+titlu+"'" + " and idprogramari = '" + MainController.selectedProgramareStatic.getIdprogramari() + "'"), Observatii.class).getResultList();
		return results.get(0);
	}
}
