package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the programari database table.
 * 
 */
@Entity
@NamedQuery(name="Programari.findAll", query="SELECT p FROM Programari p")
public class Programari implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idprogramari;

	private String data;

	private String ora;

	//bi-directional many-to-one association to Observatii
	@OneToMany(mappedBy="programari")
	private List<Observatii> observatiis;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="animalulProgramat")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="doctorulProgramat")
	private Personalmedical personalmedical;

	public Programari() {
	}

	public int getIdprogramari() {
		return this.idprogramari;
	}

	public void setIdprogramari(int idprogramari) {
		this.idprogramari = idprogramari;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getOra() {
		return this.ora;
	}

	public void setOra(String ora) {
		this.ora = ora;
	}

	public List<Observatii> getObservatiis() {
		return this.observatiis;
	}

	public void setObservatiis(List<Observatii> observatiis) {
		this.observatiis = observatiis;
	}

	public Observatii addObservatii(Observatii observatii) {
		getObservatiis().add(observatii);
		observatii.setProgramari(this);

		return observatii;
	}

	public Observatii removeObservatii(Observatii observatii) {
		getObservatiis().remove(observatii);
		observatii.setProgramari(null);

		return observatii;
	}

	public Animal getAnimal() {
		return this.animal;
	}

	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}