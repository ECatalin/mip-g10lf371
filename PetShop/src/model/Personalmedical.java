package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the personalmedical database table.
 * 
 */
@Entity
@NamedQuery(name="Personalmedical.findAll", query="SELECT p FROM Personalmedical p")
public class Personalmedical implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idpersonalmedical;

	private String specializare;

	private String veterinar;

	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="personalmedical")
	private List<Programari> programaris;

	public Personalmedical() {
	}

	public int getIdpersonalmedical() {
		return this.idpersonalmedical;
	}

	public void setIdpersonalmedical(int idpersonalmedical) {
		this.idpersonalmedical = idpersonalmedical;
	}

	public String getSpecializare() {
		return this.specializare;
	}

	public void setSpecializare(String specializare) {
		this.specializare = specializare;
	}

	public String getVeterinar() {
		return this.veterinar;
	}

	public void setVeterinar(String veterinar) {
		this.veterinar = veterinar;
	}

	public List<Programari> getProgramaris() {
		return this.programaris;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.programaris = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setPersonalmedical(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setPersonalmedical(null);

		return programari;
	}

}