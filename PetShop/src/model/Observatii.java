package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the observatii database table.
 * 
 */
@Entity
@NamedQuery(name="Observatii.findAll", query="SELECT o FROM Observatii o")
public class Observatii implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idObservatii;

	@Lob
	private String observatie;

	private String titlu;

	//bi-directional many-to-one association to Programari
	@ManyToOne
	@JoinColumn(name="idprogramari")
	private Programari programari;

	public Observatii() {
	}

	public int getIdObservatii() {
		return this.idObservatii;
	}

	public void setIdObservatii(int idObservatii) {
		this.idObservatii = idObservatii;
	}

	public String getObservatie() {
		return this.observatie;
	}

	public void setObservatie(String observatie) {
		this.observatie = observatie;
	}

	public String getTitlu() {
		return this.titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public Programari getProgramari() {
		return this.programari;
	}

	public void setProgramari(Programari programari) {
		this.programari = programari;
	}

}