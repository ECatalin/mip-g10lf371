package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the animal database table.
 * 
 */
@Entity
@NamedQuery(name="Animal.findAll", query="SELECT a FROM Animal a")
public class Animal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idanimal;

	private String dataNasterii;

	private String nume;

	@Lob
	private String observatiproprietar;

	private String rasa;

	private String sex;

	private String specie;

	//bi-directional many-to-one association to Programari
	@OneToMany(mappedBy="animal")
	private List<Programari> programaris;

	public Animal() {
	}

	public int getIdanimal() {
		return this.idanimal;
	}

	public void setIdanimal(int idanimal) {
		this.idanimal = idanimal;
	}

	public String getDataNasterii() {
		return this.dataNasterii;
	}

	public void setDataNasterii(String dataNasterii) {
		this.dataNasterii = dataNasterii;
	}

	public String getNume() {
		return this.nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getObservatiproprietar() {
		return this.observatiproprietar;
	}

	public void setObservatiproprietar(String observatiproprietar) {
		this.observatiproprietar = observatiproprietar;
	}

	public String getRasa() {
		return this.rasa;
	}

	public void setRasa(String rasa) {
		this.rasa = rasa;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getSpecie() {
		return this.specie;
	}

	public void setSpecie(String specie) {
		this.specie = specie;
	}

	public List<Programari> getProgramaris() {
		return this.programaris;
	}

	public void setProgramaris(List<Programari> programaris) {
		this.programaris = programaris;
	}

	public Programari addProgramari(Programari programari) {
		getProgramaris().add(programari);
		programari.setAnimal(this);

		return programari;
	}

	public Programari removeProgramari(Programari programari) {
		getProgramaris().remove(programari);
		programari.setAnimal(null);

		return programari;
	}

}