package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import util.DatabaseUtil;

public class DiagnosticController implements Initializable{

	@FXML
    private TextField diagTitle;

    @FXML
    private TextArea diagDescription;

    @FXML
    private Button saveButton;

    @FXML
    void saveFile(ActionEvent event) {
    	DatabaseUtil db = new DatabaseUtil();
    	db.setUp();
    	db.startTransaction();
    	db.createDiagnostic(diagTitle.getText().toString(), diagDescription.getText().toString(), MainController.selectedProgramareStatic);
    	if(diagTitle.getText().toString().length() == 0) {
    		Alert alert = new Alert(AlertType.NONE, "Please add a title", ButtonType.OK);
    		alert.showAndWait();
    	}else if(diagDescription.getText().toString().length() == 0) {
    		Alert alert = new Alert(AlertType.NONE , "Please add a description" , ButtonType.OK);
    		alert.showAndWait();
    	}else {
    		db.commitTransaction();
    		db.closeEntityManager();
    		Stage stage = (Stage) saveButton.getScene().getWindow();
    		stage.close();
    	}
    	
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		diagTitle.setPromptText("Title");
		diagDescription.setPromptText("Diagnostic");
	}

}
