package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class LogginInfo implements Initializable{
	
	String user = "Adam";
	String pass = "123456";
	
	@FXML
    private TextField password;

    @FXML
    private TextField username;
    
    @FXML
    void tryLogIn(ActionEvent event) {
    	if(user.equals(username.getText()) && pass.equals(password.getText())) {
    		Stage st = (Stage) username.getScene().getWindow();
    		st.close();
    		Parent root;
            try {
                root = FXMLLoader.load(getClass().getResource("MainView.fxml"));
                Stage stage = new Stage();
                stage.setScene(new Scene(root, 800, 800));
                stage.show();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
    	}else {
    		System.out.println("Wrong username or pass!");
    	}
    }
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}

}
