package controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Observatii;
import model.Programari;
import util.DatabaseUtil;

public class MainController implements Initializable{

	@FXML
	private ListView<String> listView;
	private String selectedTime;
	private String selectedDiag;
	public static Programari selectedProgramareStatic = new Programari();
	
	private void populateMainListView(){
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Programari> programariDBList = (List <Programari>)db.programariList();
		ObservableList<String> programariDatesList = getProgramari(programariDBList);
		listView.setItems(programariDatesList);
		listView.refresh();
		db.closeEntityManager();
	}
	public ObservableList<String> getProgramari(List<Programari> programari){
		ObservableList<String> names = FXCollections.observableArrayList();
		for(Programari a : programari) {
			if(a.getData().compareTo(today.getText().toString()) == 0) {
				if(names.size() == 0)
					selectedTime = a.getOra();
				names.add(a.getOra());
			}
		}
		return names;
	}
	
	
	//Arata informatiile pentru programarea selectata
	@FXML
    void printSelected(MouseEvent event) {
		if(listView.getSelectionModel().getSelectedItem() != selectedTime)
			selectedTime = listView.getSelectionModel().getSelectedItem();
		if(selectedTime != null) {
			populatePetInformation();
			View();
			PopulateDiagnosticsList();
		}
	}
	
	
	void populatePetInformation() {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		Programari selectedProgram = db.getselectedProgramare(today.getText().toString(), selectedTime);
		selectedProgramareStatic = selectedProgram;
		nume.setText("Nume: "+selectedProgram.getAnimal().getNume());
		specia.setText("Specia: " + selectedProgram.getAnimal().getSpecie());
		rasa.setText("Rasa: " + selectedProgram.getAnimal().getRasa());
		LocalDate now = LocalDate.now();
		LocalDate age = LocalDate.parse(selectedProgram.getAnimal().getDataNasterii());
		String varstaFinala = "Varsta: ";
		if((now.getYear() - age.getYear()) == 1){
			varstaFinala += "Un an ";
		}else if((now.getYear() - age.getYear()) > 1) {
			varstaFinala += (now.getYear() - age.getYear()) + " ani ";
		}
		if((now.getMonthValue() - age.getMonthValue()) == 1){
			varstaFinala += "si o luna";
		}else if((now.getMonthValue() - age.getMonthValue()) > 1) {
			varstaFinala += "si " + (now.getMonthValue() - age.getMonthValue()) + " luni";
		}
		observatiProprietar.setText(selectedProgram.getAnimal().getObservatiproprietar());
		varsta.setText(varstaFinala);
		db.closeEntityManager();
		PopulateDiagnosticsList();
		View();
	}

    @FXML
    private TextArea observatiProprietar;
    
    @FXML
    private Button addDiagnostic;

    @FXML
    void addDiagnosticTab(ActionEvent event) {
    	Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("DiagnosticFXML.fxml"));
            Stage stage = new Stage();
            stage.setTitle("Diagnostic");
            stage.setScene(new Scene(root, 450, 450));
            stage.show();
            stage.setOnHiding( event2 -> {PopulateDiagnosticsList();} );
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    @FXML
    private ListView<String> diagnostice;
    
    @FXML
    void viewDiagnostic(ActionEvent event) {
    	DatabaseUtil db = new DatabaseUtil();
    	db.setUp();
    	db.startTransaction();
    	Observatii diag = db.getSelectedDiag(selectedDiag);
    	db.closeEntityManager();
    	Alert diagView = new Alert(AlertType.NONE , diag.getObservatie(), ButtonType.OK);
    	diagView.showAndWait();
    }
    private void PopulateDiagnosticsList() {
    	DatabaseUtil db = new DatabaseUtil();
    	db.setUp();
    	db.startTransaction();
    	List<Observatii> observatiDBList = (List<Observatii>)db.diagList();
    	ObservableList<String> observatiList = getObservatiiList(observatiDBList);
    	diagnostice.setItems(observatiList);
    	diagnostice.refresh();
    	db.closeEntityManager();
    }
    private ObservableList<String> getObservatiiList(List<Observatii> list){
    	ObservableList<String> lista = FXCollections.observableArrayList();
    	for(Observatii a : list) {
    		if(a.getProgramari().getIdprogramari() == selectedProgramareStatic.getIdprogramari())
    			lista.add(a.getTitlu());
    	}
    	return lista;
    }
    @FXML
    void selectDiagnostic(MouseEvent event) {
    	selectedDiag = diagnostice.getSelectionModel().getSelectedItem();
    }
	//Selecteaza o data cu datePicker
	@FXML
	private DatePicker datePicker;
	@FXML
	void selectedDate(ActionEvent event) {
		LocalDate i = datePicker.getValue();
		today.setText(i.toString());
		populateMainListView();
		populatePetInformation();
	}

    @FXML
    private Text today;
    private void setToday() {
    	today.setText(LocalDate.now().toString());
    	datePicker.setPromptText("Select Date");
    }
    
    @FXML
    private Text nume;

    @FXML
    private Text varsta;

    @FXML
    private Text specia;

    @FXML
    private Text rasa;

    @FXML
    private Text proprietar;
	
    @FXML
    private Pane pane;
    
    @FXML
    private ImageView image;
    void View() {
    	Image img = new Image(MainController.class.getResourceAsStream("IMG.jpg"));
    	image.setImage(img);
    }
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
			setToday();
			try {
				populateMainListView();
				populatePetInformation();
				PopulateDiagnosticsList();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
